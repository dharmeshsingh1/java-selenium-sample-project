package com.willysalazar.example.driver;

import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Arrays;

class ChromeOptionsFactory {

    ChromeOptions build(){

        ChromeOptions optionsChrome = new ChromeOptions();
        optionsChrome.addArguments("headless");
        optionsChrome.addArguments("start-maximized");
        optionsChrome.addArguments("lang=pt-BR");
        optionsChrome.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors,--web-security=false,--ssl-protocol=any,--ignore-ssl-errors=true"));
        optionsChrome.setAcceptInsecureCerts(true);
        optionsChrome.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        optionsChrome.addArguments("--no-sandbox"); 
        return optionsChrome;
    }
}
